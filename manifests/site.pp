node default {
	file {'/root/README':
		ensure 	=> file,
		content	=> 'This is some content, also I\'m changing the owner to Vagrant\n',
		owner 	=> 'root',
	}
  file {'/root/mytestfile':
    ensure => file,
    content => 'This only a test from outerspace',
    owner => 'vagrant';
  }
  package { 'httpd':
    ensure => present,
  } 
  #file { '/root/.vimrc':
  #  ensure => file,
  #  source => 'puppet:///modules/production/vimrc',
  #}
}
